<?php
/**
 * File Description [ Level 1 Block ]
 * 
 * File descriptions outline the purpose for the file and 
 * make it easier to distinguish what types of things
 * belong in the file or need to be placed somewhere else.
 * 
 * Requirements - specifies what type of services, other
 * scripts, libraries, or dependencies the content within
 * this document require, the variable names they should 
 * be assigned and/or instructions regarding how to 
 * configure.
 * 
 * Examples: PHP Version, required extensions or packages
 * 
 * License - Describe how others may or may not use this
 * document within their own projects.
 * 
 * @category   CategoryName
 * @package    PackageName
 * @author     Original Author <author@example.com>
 * @author     Another Author <another@example.com>
 * @copyright  1997-2005 The PHP Group
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    1.0.1
 * @link       http://pear.php.net/package/PackageName
 * @since      File available since Release 1.2.0
 * @deprecated File deprecated in Release 2.0.0
 * @todo
 * @var
 * 
 * Globally Accessible Variabls - provides the names of variables
 * declared within the document that globally accessible. 
 * 
 * Example:
 * @global int $counter_for_something
 * 
 * 
 */

 // {{{ constants

// }}}
// {{{ GLOBALS

// }}}


/*
     _______..______      ___       ______  _______ 
    /       ||   _  \    /   \     /      ||   ____|
   |   (----`|  |_)  |  /  ^  \   |  ,----'|  |__   
    \   \    |   ___/  /  /_\  \  |  |     |   __|  
.----)   |   |  |     /  _____  \ |  `----.|  |____ 
|_______/    | _|    /__/     \__\ \______||_______|
                                                    
*/

// Use elseif not else if



?>